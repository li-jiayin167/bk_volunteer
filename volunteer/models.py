from django.db import models
from django.db.models.base import Model
from blueapps.account.models import User
# Create your models here.
import settings

class UserAbstractModel(models.Model):
    """
    学生角色、公益企业角色和管理员角色的抽象类
    """
    ROLE_CHOICES = {
        (0, '学生'),
        (1, '公益企业'),
        (2, '管理员')
    }
    
    username = models.CharField('用户名', default='', max_length=50, null=False)
    password = models.CharField('密码', default='', max_length=50, null=False)
    created = models.DateTimeField('注册时间', auto_now_add=True, null=False)

    class Meta:
        abstract = True

# 学生角色的模型类
class Student(UserAbstractModel):
    """
    学生角色的模型类
    """
   
    role = models.IntegerField(verbose_name='用户角色',default='0',choices=UserAbstractModel.ROLE_CHOICES,null=False)

    def __str__(self):
        return self.username
        

# 公益企业
class Company(UserAbstractModel):
    """
    公益企业角色的模型类
    """
    role = models.IntegerField(verbose_name='用户角色',default='1',choices=UserAbstractModel.ROLE_CHOICES, null=False)
   
    

    def __str__(self):
        return self.username
        

# 管理员
class Manager(UserAbstractModel):
    """
    管理员的模型类
    """
    role = models.IntegerField(verbose_name='用户角色',default='2',choices=UserAbstractModel.ROLE_CHOICES, null=False)


    def __str__(self):
        return self.username


# 发布活动表
class Activity(models.Model):

    publish_company_name = models.CharField(verbose_name="发布企业", max_length=50)
    publish_demand = models.CharField(verbose_name="活动要求", max_length=100)
    publish_activity_name = models.CharField(verbose_name="所属活动", max_length=50, db_index=True)
    publish_datetime = models.CharField(verbose_name="活动时间", max_length=100)
    publish_location = models.CharField(verbose_name="活动地点", max_length=50)
    publish_person_num = models.IntegerField(verbose_name="志愿者人数")
    apply_person_num = models.IntegerField(verbose_name="已报名人数", default=0)
    pass_person_num = models.IntegerField(verbose_name="已通过人数", default=0)
    create_time = models.DateTimeField(verbose_name="发布时间", auto_now_add=True)

    class Meta:
        verbose_name = "志愿者公益活动列表"
        verbose_name_plural = verbose_name
        ordering = ["-create_time"]
        db_table = "bk_volunteer_activity"
   

    def __str__(self):
        return self.publish_activity_name

#报名状态的模型类
class ApplyStatus(models.Model): 
    WAIT = 0   #待审核
    PASS = 1   #已审核
    FAIL = 2   #未通过

# 报名
class Apply(models.Model):
    SEX = [(1, '男'), (2, '女')]
    GRADE_LIST = [
        (1, "大一"),
        (2, "大二"),
        (3, "大三"),
        (4, "大四"),
        (5, "研一"),
        (6, "研二"),
        (7, "研三"),
    ]
    STATUS = [
        (ApplyStatus.PASS, "已审核"),
        (ApplyStatus.WAIT, "待审核"),
        (ApplyStatus.FAIL, "未通过"), 
    ]

    belonging_activity = models.ForeignKey(Activity, models.CASCADE)
    apply_name = models.CharField(verbose_name="姓名", max_length=50)
    apply_age = models.IntegerField(verbose_name="年龄")
    apply_sex = models.IntegerField(verbose_name="性别", choices=SEX, default=1)
    apply_school = models.CharField(verbose_name="学校", max_length=100)
    apply_grade = models.IntegerField(verbose_name="年级", choices=GRADE_LIST, default=1)
    apply_status = models.IntegerField(verbose_name="报名状态", choices=STATUS, default=ApplyStatus.WAIT)
    apply_time = models.DateTimeField(verbose_name="报名时间", auto_now_add=True)

    class Meta:
        verbose_name = "报名信息表"
        verbose_name_plural = verbose_name
        ordering = ["-apply_time"]
        db_table = "bk_volunteer_apply"

    def __str__(self):
        return self.apply_name
