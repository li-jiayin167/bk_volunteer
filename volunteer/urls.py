from django.conf.urls import url
from . import views

urlpatterns = (
    # url(r'^base/', views.base),
    url(r'^$', views.login),
    url(r'^login_authentication/$', views.login_authentication),
    url(r'^register/$', views.register),
    url(r'^checkApply/', views.checkApply),
    url(r'^switch_apply_status/', views.switch_apply_status),
    url(r'^myApply/', views.myApply),
    url(r'^studentHome/', views.studentHome),
    url(r'^companyHome/', views.companyHome),
    url(r'^userManagement/', views.userManagement),

    url(r'^summaryActivity/',views.summaryActivity),
    url(r'^get_activity_display_info/(?P<activity_id>\d+)/', views.get_activity_display_info_by_id),
    url(r'^get_apply_list/$', views.get_apply_list),
    url(r'^get_student_info_list/$', views.get_student_info_list),
    url(r'^get_company_info_list/$', views.get_company_info_list),
    url(r'^delete_activity_info/(?P<activity_id>\d+)/', views.delete_activity_info),
    url(r'^delete_student_info/(?P<student_id>\d+)/', views.delete_student_info),
    url(r'^delete_company_info/(?P<company_id>\d+)/', views.delete_company_info),
    url(r'^reset_student_password/(?P<student_id>\d+)/', views.reset_student_password),
    url(r'^reset_company_password/(?P<company_id>\d+)/', views.reset_company_password),
    url(r'^get_apply_info/(?P<apply_id>\d+)/', views.get_apply_info_by_id),    #编辑报名信息
    url(r'^get_activity_select_data/$', views.get_activity_select_data),
    url(r'^save_apply_info/$', views.save_apply_info),
    url(r'^save_publish_info/$',views.save_publish_info),
    url(r"^get_activity_list/$", views.get_activity_list),
   
    
    
)
