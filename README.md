开发框架2.0使用说明：https://docs.bk.tencent.com/blueapps/USAGE.html

SaaS链接：https://paas-edu.bktencent.com/o/bk-volunteer/

Gitee：https://gitee.com/li-jiayin167/bk_volunteer.git

**说明**：用户在登录访问时，学生和公益企业账号可以自行注册登录。由于管理员账号不可以进行注册，如若要访问管理员页面，请登录以下任一账号（登录时选择身份为管理员）：

| 账号     | 密码     |
| -------- | -------- |
| manager1 | manager1 |
| manager2 | manager2 |
| manager3 | manager3 |
| manager4 | manager4 |

